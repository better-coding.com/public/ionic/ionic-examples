import { Component } from '@angular/core';
import { BackButtonService } from '../services/back-button.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public backButtonService:BackButtonService) {}
}
