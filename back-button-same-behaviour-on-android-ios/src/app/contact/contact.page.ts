import { Component, OnInit } from '@angular/core';
import { BackButtonService } from '../services/back-button.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(public backButtonService:BackButtonService) { }

  ngOnInit() {
  }

}
