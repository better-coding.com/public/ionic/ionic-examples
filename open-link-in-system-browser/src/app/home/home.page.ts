import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

  openLinkInSystemBrowser(url: string) {
    window.open(url, '_system', 'location=yes');
  }
  
}
